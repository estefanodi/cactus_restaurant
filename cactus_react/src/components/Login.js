import React from 'react'

import '../styles/App.css'

export default class Login extends React.Component{
  state = { 
    email : '',
    pass  : ''
  }

  handleChange = (e) => {
    this.setState({[e.target.name]:e.target.value})
  }

  handleSubmit = (e) => {
    e.preventDefault()
    let { email,pass } = this.state;
    this.props.login(email,pass)
  }
  

	render(){
		return(
      <div className='main-container'>
        <form className = 'form-container' 
              onSubmit  = {this.handleSubmit.bind(this)}
              onChange  = {this.handleChange.bind(this)}>
         <div className='input-container'>
          <input
            name   = 'email'
            value  = { this.state.email }
            type   = 'email'
            required
          />
         </div>
         <div className='input-container'>
          <input
            name   = 'pass'
            value  = { this.state.pass }
            type   = 'password'
            required
          />
         </div>
         <div className='input-container'>
            <button className='submit-button'>ENTRAR</button>
         </div>
        </form>
      </div>
	    )
	}
}