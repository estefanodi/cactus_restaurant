import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyCe37Lc0Fa7o3FgOQijODfm5qKcIR7UZ80",
    authDomain: "cactus-6c9e3.firebaseapp.com",
    databaseURL: "https://cactus-6c9e3.firebaseio.com",
    projectId: "cactus-6c9e3",
    storageBucket: "cactus-6c9e3.appspot.com",
    messagingSenderId: "817420100445"
};
var fire = firebase.initializeApp(config);
export const auth = firebase.auth();
export default fire;
