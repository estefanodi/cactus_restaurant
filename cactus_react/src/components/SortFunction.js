export default (data)=>{ 
   let arr  = [];


   for(let key in data){
   	    let yea  = data[key].fecha.substring(0, 4);
		let mon  = data[key].fecha.substring(5, 7)-1;
		let day  = data[key].fecha.substring(8, 10);
		let hou  = data[key].hora.substring(0, 2);
		let min  = data[key].hora.substring(3, 5);
   	    let date = new Date(yea,mon,day,hou,min)
	  	let obj  = {
	  		_id        : key,
	  		nombre     : data[key].nombre,
	  		comensales : data[key].comensales,
	  		telefono   : data[key].telefono,
	  		fecha      : data[key].fecha,
	  		hora       : data[key].hora,
	  		email      : data[key].email,
	  		approved   : data[key].approved,
	  		display    : data[key].display,
	  		uid        : data[key].uid,
            date 
        }
	  	arr.push(obj)
	  }
   arr.sort(function compare(a, b) {
	  let dateA = new Date(a.date);
	  let dateB = new Date(b.date);
	  return dateA - dateB;
	});
	  return  arr
}




