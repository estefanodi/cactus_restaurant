import React        from 'react'
import Login        from './Login'
import Bookings       from './Bookings'
import firebase     from 'firebase' 
import fire         from '../fire'
import SortFunction from './SortFunction'

import '../styles/App.css'


export default class App extends React.Component{
  state = {login:true}

componentWillMount(){
  let data;
	let reservas = fire.database().ref('reservas');

	reservas.on("value", (snapshot) =>{
	  
	  let data = SortFunction(snapshot.val())
	  this.setState({data})
      
	},(errorObject) =>{
	  console.log("****************" + errorObject.code);
	});
 }

//********************************************LOGIN*********************************************  
  login = (email,password) => {
  	firebase.auth().signInWithEmailAndPassword(email,password).catch(function(error) {
       var errorMessage = error.message
	     if (error) alert(errorMessage)
	})
	var user = firebase.auth().currentUser;

	user ? this.setState({login:!this.state.login}) : null
  }
//********************************************LOGOUT******************************************** 
  logout = () => {
  	firebase.auth().signOut().then(()=>{
        this.setState({login:!this.state.login})
     }).catch((error)=>{
        console.log(error)
    });
 }
//********************************************UPDATE********************************************

 update = (app,dis,data) => {
    console.log(data)
    let reservas = fire.database().ref('reservas');
    reservas.update({
      [data._id]:{
         approved   : app,
         display    : dis,
         nombre     : data.nombre,
         comensales : data.comensales,
         telefono   : data.telefono,
         email      : data.email,
         fecha      : data.fecha,
         hora       : data.hora,
         uid        : data.uid
      }
    })
 }
//**********************************************************************************************
  render(){

  	let { login } = this.state, show;

  	   login 
  	 ? show = <Login  login={this.login}/>
  	 : show = <Bookings reservas={this.state.data} logout={this.logout} update={this.update}/>		  
  		
  	 return  <div>{show}</div>
  	
  }
}
