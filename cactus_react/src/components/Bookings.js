import React from 'react'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css

import '../styles/App.css'

export default class Bookings extends React.Component{
  state = {
    orange : 'material-icons md-36 orange',
    green  : 'material-icons md-36 green'
  }

  open_alert = (reserva) => {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='custom-ui'>
            <div className='custom-ui-in'>
                <p className='title'>Cancela Reserva</p></div>
            <div className='custom-ui-in'>
                <p className='content'>Cancela la reserva de { reserva.nombre}  a  las  {reserva.hora}</p></div>
            <div className='custom-ui-buttons'><button className="alertButton" onClick={onClose}>NO</button>
                  <button className="alertButton" onClick={() => {
                      this.props.update('rechazado',false,reserva);
                      onClose()
                  }}>SI</button>
            </div>
          </div>
        )
      }
    })
  }

	render(){
    let icon,color;
		return(
          <div className='main-orders-container'>
            <div className='table-row'  style={{backgroundColor:'#9fc4c6'}}>
               <div className='table-in'>NOMBRE</div>
               <div className='table-in'>PERSONAS</div>
               <div className='table-in'>TELEFONO</div>
               <div className='table-in'>EMAIL</div>
               <div className='table-in'>FECHA</div>
               <div className='table-in'>HORA</div>
               <div className='table-in'>CONFIRMADO</div>
               <div className='table-in'></div>
               <div className='table-in'>
                  <button className='logout_button'
                          onClick={()=>this.props.logout()}>Logout</button>
               </div>
               <div className='table-in'></div>
            </div>
            {
              this.props.reservas.map( (ele,i) =>{
                this.props.reservas
                debugger
                if(ele.approved === 'confirmado'){
                  icon = 'check' ; color = this.state.green;
                }else{
                  icon = 'query_builder' ; color = this.state.orange;
                }
                if(ele.display === true){
                   return <div className='table-row' key={i}>
                           <div className='table-in text'>{ele.nombre}</div>
                           <div className='table-in text'>{ele.comensales}</div>
                           <div className='table-in text'>{ele.telefono}</div>
                           <div className='table-in text'>{ele.email}</div>
                           <div className='table-in text'>{ele.fecha}</div>
                           <div className='table-in text'>{ele.hora}</div>
                           <div className='table-in text'><i className={color}>{icon}</i></div>
                           
                           <button className = 'confirm-button'
                                      onClick   = {()=>this.props.update('confirmado',true,ele)} 
                              ><i className="material-icons md-36 check">check</i></button>
                            
                          <button className = 'confirm-button'
                                  onClick   = {()=>this.props.update('pendiente',true,ele)}
                          ><i className="material-icons md-36 builder">query_builder</i></button>

                          <button className = 'confirm-button'
                                  onClick   = {()=>this.open_alert(ele)}
                          ><i className="material-icons md-36 remove">clear</i></button>

                        </div>

                }else null
              })
            }


            
          </div>
	    )
	}
}